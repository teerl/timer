package main

import (
	"fmt"
	"time"
	"os"
	"regexp"
)

const clearLine = "\x1b[2K\x1b[G"

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Need a duration")
		return
	}
	sleepTime, err := time.ParseDuration(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	tickTime := 100 * time.Millisecond
	if len(os.Args) > 2 {
		tickTime, err = time.ParseDuration(os.Args[2])
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	doneSignal := time.After(sleepTime)
	doneTime := time.Now().Add(sleepTime + 100 * time.Millisecond)
	timeRe := regexp.MustCompile(`(\.\d)\d*s`)
	ticker := time.Tick(tickTime)
	
	fmt.Print(timeRe.ReplaceAllString(sleepTime.String(), `${1}s`))
	MainFor:
	for {
		select {
		case <-doneSignal:
			break MainFor
		case now := <-ticker:
			msg := timeRe.ReplaceAllString(doneTime.Sub(now).String(), `${1}s`)
			fmt.Print(clearLine, msg)
		}
	}
	fmt.Print(clearLine)
}
